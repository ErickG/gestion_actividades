package util;

import java.util.List;

public interface DAO<T> {

    public void create(T e);

    public void remove(T e);

    public void edit(T e);

    public T find(Object e);

    public List<T> findAll();
}
