package config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import impl.ActividadesImpl;
import impl.DetFacesImpl;
import impl.EstadosImpl;
import impl.FacesImpl;
import impl.FechaEspecialImpl;
import impl.PersonasImpl;
import impl.TipoUsuariosImpl;
import impl.UsuariosImpl;
import org.apache.commons.dbcp2.BasicDataSource;


@Configuration
@ComponentScan(basePackages = {"src"})
@EnableTransactionManagement(proxyTargetClass=true)
public class AppContextConfig {

	@Bean(name="dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/gestion?useSSL=false");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}
	
	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.format_sql", "hibernate.format_sql");
		return properties;
	}
	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("entity");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}
	@Bean
	public ActividadesImpl actividadDao() {
		return new ActividadesImpl();
	}
	@Bean
	public DetFacesImpl detFaceDao() {
		return new DetFacesImpl();
	}
	@Bean
	public EstadosImpl estadoDao() {
		return new EstadosImpl();
	}
	@Bean
	public FacesImpl faceDao() {
		return new FacesImpl();
	}
	@Bean
	public FechaEspecialImpl fEspecialDao() {
		return new FechaEspecialImpl();
	}
	@Bean
	public PersonasImpl personaDao() {
		return new PersonasImpl();
	}
	@Bean
	public TipoUsuariosImpl tUsuarioDao() {
		return new TipoUsuariosImpl();
	}
	@Bean
	public UsuariosImpl usuarioDao() {
		return new UsuariosImpl();
	}
}
