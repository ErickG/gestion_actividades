package ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.Usuarios;
import impl.UsuariosImpl;

@SuppressWarnings("serial")
@Component
@ManagedBean
@SessionScoped
public class UsuariosC implements Serializable {

	@Autowired
	private UsuariosImpl usuarioDao;
	private Usuarios usuario;
	private List<Usuarios> listaUsuarios;
	String mensaje="";
	public Usuarios getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	public List<Usuarios> getListaUsuarios() {
		listaUsuarios=usuarioDao.findAll();
		return listaUsuarios;
	}
	public void setListaUsuarios(List<Usuarios> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	
	@PostConstruct
	public void init() {
		usuario=new Usuarios();
	}
	public void crearUsuario() {
		try {
			usuarioDao.create(usuario);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void editarUsuario() {
		try {
			usuarioDao.edit(usuario);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Error al editar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void deleteUsuario(Usuarios us) {
		try {
			usuarioDao.remove(us);
			mensaje = "Registro Eliminado";
		} catch (Exception e) {
			mensaje="Error al eliminar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void cargarUsuario(Usuarios us) {
		usuario =us;
	}
}
