package ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.Estados;
import entity.Faces;
import entity.FechaEspecial;
import entity.TipoUsuarios;
import impl.EstadosImpl;
import impl.FacesImpl;
import impl.FechaEspecialImpl;
import impl.TipoUsuariosImpl;

@SuppressWarnings("serial")
@Component
@ManagedBean
@SessionScoped
public class Catalogos implements Serializable {
@Autowired
private EstadosImpl estadoDao;
@Autowired
private FacesImpl faceDao;
@Autowired
private FechaEspecialImpl fechaEspecialDao;
@Autowired
private TipoUsuariosImpl tipoUsuarioDao;

private Estados estado;
private Faces face;
private FechaEspecial fechaEspecial;
private TipoUsuarios tipoUsuario;

private List<Estados> listaEstados;
private List<Faces> listaFaces;
private List<FechaEspecial> listaFechaEspecial;
private List<TipoUsuarios> listaTipoUsuarios;

String mensaje="";

public Estados getEstado() {
	return estado;
}
public void setEstado(Estados estado) {
	this.estado = estado;
}
public Faces getFace() {
	return face;
}
public void setFace(Faces face) {
	this.face = face;
}
public FechaEspecial getFechaEspecial() {
	return fechaEspecial;
}
public void setFechaEspecial(FechaEspecial fechaEspecial) {
	this.fechaEspecial = fechaEspecial;
}
public TipoUsuarios getTipoUsuario() {
	return tipoUsuario;
}
public void setTipoUsuario(TipoUsuarios tipoUsuario) {
	this.tipoUsuario = tipoUsuario;
}
public List<Estados> getListaEstados() {
	listaEstados=estadoDao.findAll();
	return listaEstados;
}
public void setListaEstados(List<Estados> listaEstados) {
	this.listaEstados = listaEstados;
}
public List<Faces> getListaFaces() {
	listaFaces=faceDao.findAll();
	return listaFaces;
}
public void setListaFaces(List<Faces> listaFaces) {
	this.listaFaces = listaFaces;
}
public List<FechaEspecial> getListaFechaEspecial() {
	listaFechaEspecial=fechaEspecialDao.findAll();
	return listaFechaEspecial;
}
public void setListaFechaEspecial(List<FechaEspecial> listaFechaEspecial) {
	this.listaFechaEspecial = listaFechaEspecial;
}
public List<TipoUsuarios> getListaTipoUsuarios() {
	listaTipoUsuarios=tipoUsuarioDao.findAll();
	return listaTipoUsuarios;
}
public void setListaTipoUsuarios(List<TipoUsuarios> listaTipoUsuarios) {
	this.listaTipoUsuarios = listaTipoUsuarios;
}

@PostConstruct
public void init() {
	estado = new Estados();
	face = new Faces();
	fechaEspecial = new FechaEspecial();
	tipoUsuario = new TipoUsuarios();
}
/*CRUD Faces*/
public void crearFace() {
	try {
		faceDao.create(face);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Debe llenar todos los campos";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void editarFace() {
	try {
		faceDao.edit(face);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Error al editar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void deleteFace(Faces fa) {
	try {
		faceDao.remove(fa);
		mensaje = "Registro Eliminado";
	} catch (Exception e) {
		mensaje="Error al eliminar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void cargarFace(Faces fa) {
	face =fa;
}

/*CRUD de Estados*/
public void crearEstado() {
	try {
		estadoDao.create(estado);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Debe llenar todos los campos";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void editarEstado() {
	try {
		estadoDao.edit(estado);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Error al editar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void deleteEstado(Estados es) {
	try {
		estadoDao.remove(es);
		mensaje = "Registro Eliminado";
	} catch (Exception e) {
		mensaje="Error al eliminar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void cargarEstado(Estados es) {
	estado =es;
}

/*CRUD de fecha especial*/
public void crearFEspecial() {
	try {
		fechaEspecialDao.create(fechaEspecial);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Debe llenar todos los campos";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void editarFEspecial() {
	try {
		fechaEspecialDao.edit(fechaEspecial);	
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Error al editar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void deleteFEspecial(FechaEspecial fe) {
	try {
		fechaEspecialDao.remove(fe);
		mensaje = "Registro Eliminado";
	} catch (Exception e) {
		mensaje="Error al eliminar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void cargarFEspecial(FechaEspecial fe) {
	fechaEspecial =fe;
}

/*CRUD de Tipo Usuarios*/
public void crearTUsuarios() {
	try {
		tipoUsuarioDao.create(tipoUsuario);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Debe llenar todos los campos";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void editarTUsuario() {
	try {
		tipoUsuarioDao.edit(tipoUsuario);
		mensaje = "Proceso Exitoso";
	} catch (Exception e) {
		mensaje="Error al editar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void deleteTUsuario(TipoUsuarios tu) {
	try {
		tipoUsuarioDao.remove(tu);
		mensaje = "Registro Eliminado";
	} catch (Exception e) {
		mensaje="Error al eliminar";
		e.printStackTrace();
	}
	FacesMessage msj = new FacesMessage(this.mensaje);
	FacesContext.getCurrentInstance().addMessage(null, msj);
}
public void cargarTUsuario(TipoUsuarios tu) {
	tipoUsuario =tu;
}
}
