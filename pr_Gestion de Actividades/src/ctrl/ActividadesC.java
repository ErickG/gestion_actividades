package ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.Actividades;
import entity.Estados;
import entity.Usuarios;
import impl.ActividadesImpl;
import impl.EstadosImpl;
import impl.UsuariosImpl;

@SuppressWarnings("serial")
@Component
@ManagedBean
@SessionScoped
public class ActividadesC implements Serializable {

	@Autowired
	private ActividadesImpl actividadDao;
	private UsuariosImpl usuarioDao;
	private EstadosImpl estadoDao;
	
	private Actividades actividad;
	private Usuarios usuario;
	private Estados estado;
	
	private List<Actividades> listaActividades;
	private List<Usuarios> listaUsuarios;
	private List<Estados> listaEstados;
	String mensaje="";
	
	public Actividades getActividad() {
		return actividad;
	}
	public void setActividad(Actividades actividad) {
		this.actividad = actividad;
	}
	public Usuarios getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	public Estados getEstado() {
		return estado;
	}
	public void setEstado(Estados estado) {
		this.estado = estado;
	}
	public List<Actividades> getListaActividades() {
		listaActividades= actividadDao.findAll();
		return listaActividades;
	}
	public void setListaActividades(List<Actividades> listaActividades) {
		this.listaActividades = listaActividades;
	}
	public List<Usuarios> getListaUsuarios() {
		System.out.println("********************--");
		listaUsuarios=usuarioDao.findAll();
		System.out.println(listaUsuarios.size()+"**************************************************");
		return listaUsuarios;
	}
	public void setListaUsuarios(List<Usuarios> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	public List<Estados> getListaEstados() {
		listaEstados=estadoDao.findAll();
		return listaEstados;
	}
	public void setListaEstados(List<Estados> listaEstados) {
		this.listaEstados = listaEstados;
	}
	
	@PostConstruct
	public void init() {
		actividad= new Actividades();
		usuario = new Usuarios();
		estado= new Estados();
	}
	public void crearActividad() {
            System.out.println("**sadg01****");
		try {
                    System.out.println("**sadg02****");
			actividad.setUsuarios(usuario);
			actividad.setEstados(estado);
			actividadDao.create(actividad);
                        System.out.println("**sadg03****");
			mensaje = "Proceso Exitoso";
                        
		} catch (Exception e) {
			mensaje="Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void editarActividad() {
		try {
			actividad.setUsuarios(usuario);
			actividad.setEstados(estado);
			actividadDao.edit(actividad);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Error al editar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void deleteActividad(Actividades ac) {
		try {
			actividadDao.remove(ac);
			mensaje = "Registro Eliminado";
		} catch (Exception e) {
			mensaje="Error al eliminar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void cargarActividad(Actividades ac) {
		actividad =ac;
	}
}
