package ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.DetFaces;
import impl.DetFacesImpl;

@SuppressWarnings("serial")
@Component
@ManagedBean
@SessionScoped
public class DetFacesC implements Serializable {

	@Autowired
	private DetFacesImpl detFaceDao;
	private DetFaces detFace;
	private List<DetFaces> listaDetFaces;
	String mensaje="";
	
	public DetFaces getDetFace() {
		return detFace;
	}
	public void setDetFace(DetFaces detFace) {
		this.detFace = detFace;
	}
	public List<DetFaces> getListaDetFaces() {
		listaDetFaces=detFaceDao.findAll();
		return listaDetFaces;
	}
	public void setListaDetFaces(List<DetFaces> listaDetFaces) {
		this.listaDetFaces = listaDetFaces;
	}
	
	@PostConstruct
	public void init() {
		detFace = new DetFaces();
	}
	public void crearDFace() {
		try {
			detFaceDao.create(detFace);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void editarDFace() {
		try {
			detFaceDao.edit(detFace);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Error al editar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void deleteDFace(DetFaces df) {
		try {
			detFaceDao.remove(df);
			mensaje = "Registro Eliminado";
		} catch (Exception e) {
			mensaje="Error al eliminar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void cargarDFace(DetFaces df) {
		detFace =df;
	}
}
