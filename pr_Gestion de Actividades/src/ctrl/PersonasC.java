package ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.Personas;
import impl.PersonasImpl;

@SuppressWarnings("serial")
@Component
@ManagedBean
@SessionScoped
public class PersonasC implements Serializable {
	@Autowired
	private PersonasImpl personaDao;
	private Personas persona;
	private List<Personas> listapersonas;
	String mensaje="";
	
	
	public Personas getPersona() {
		return persona;
	}
	public void setPersona(Personas persona) {
		this.persona = persona;
	}
	public List<Personas> getListapersonas() {
		listapersonas=personaDao.findAll();
		return listapersonas;
	}
	public void setListapersonas(List<Personas> listapersonas) {
		this.listapersonas = listapersonas;
	}
	
	@PostConstruct
	public void init() {
		persona = new Personas();
	}
	
	public void insertarPersona() {
		try {
			personaDao.create(persona);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void editarPersona() {
		try {
			personaDao.edit(persona);
			mensaje = "Proceso Exitoso";
		} catch (Exception e) {
			mensaje="Error al editar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void eliminarPersona(Personas p) {
		try {
			personaDao.remove(p);
			mensaje = "Registro eliminado";
		} catch (Exception e) {
			mensaje="Error al eliminar el registro";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	public void consultarPersona(Personas p) {
		persona =p;
	}
}
