package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.FechaEspecial;
import util.AbstractFacade;
import util.DAO;

public class FechaEspecialImpl extends AbstractFacade<FechaEspecial> implements DAO<FechaEspecial> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public FechaEspecialImpl(SessionFactory sessionFactory, Class<FechaEspecial> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public FechaEspecialImpl() {
		super(FechaEspecial.class);
	}
}
