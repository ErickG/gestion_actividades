package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.DetFaces;
import util.AbstractFacade;
import util.DAO;

public class DetFacesImpl extends AbstractFacade<DetFaces> implements DAO<DetFaces> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public DetFacesImpl(SessionFactory sessionFactory, Class<DetFaces> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public DetFacesImpl() {
		super(DetFaces.class);
	}
}
