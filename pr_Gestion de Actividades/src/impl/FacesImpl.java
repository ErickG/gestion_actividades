package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.Faces;
import util.AbstractFacade;
import util.DAO;

public class FacesImpl extends AbstractFacade<Faces> implements DAO<Faces> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public FacesImpl(SessionFactory sessionFactory, Class<Faces> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public FacesImpl() {
		super(Faces.class);
	}
}
