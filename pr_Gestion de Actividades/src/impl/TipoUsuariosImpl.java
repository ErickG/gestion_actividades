package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.TipoUsuarios;
import util.AbstractFacade;
import util.DAO;

public class TipoUsuariosImpl extends AbstractFacade<TipoUsuarios> implements DAO<TipoUsuarios> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public TipoUsuariosImpl(SessionFactory sessionFactory, Class<TipoUsuarios> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public TipoUsuariosImpl() {
		super(TipoUsuarios.class);
	}
}