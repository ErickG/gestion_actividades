package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import entity.Actividades;
import util.AbstractFacade;
import util.DAO;

@Repository("actividades")
public class ActividadesImpl extends AbstractFacade<Actividades> implements DAO<Actividades> {

	@Autowired
	private SessionFactory sessionFactory;
	
	public ActividadesImpl(SessionFactory sessionFactory, Class<Actividades> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public ActividadesImpl() {
		super(Actividades.class);
	}
	
}
