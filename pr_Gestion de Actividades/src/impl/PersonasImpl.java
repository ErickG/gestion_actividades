package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.Personas;
import util.AbstractFacade;
import util.DAO;

public class PersonasImpl extends AbstractFacade<Personas> implements DAO<Personas> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public PersonasImpl(SessionFactory sessionFactory, Class<Personas> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public PersonasImpl() {
		super(Personas.class);
	}
}