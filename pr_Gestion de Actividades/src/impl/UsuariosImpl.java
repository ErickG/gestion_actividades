package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.Usuarios;
import util.AbstractFacade;
import util.DAO;

public class UsuariosImpl extends AbstractFacade<Usuarios> implements DAO<Usuarios> {

	@Autowired
	private SessionFactory sessionFactory;
	
	public UsuariosImpl(SessionFactory sessionFactory, Class<Usuarios> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public UsuariosImpl() {
		super(Usuarios.class);
	}
	
}
