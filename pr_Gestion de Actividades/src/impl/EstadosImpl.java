package impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.Estados;
import util.AbstractFacade;
import util.DAO;

public class EstadosImpl extends AbstractFacade<Estados> implements DAO<Estados> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public EstadosImpl(SessionFactory sessionFactory, Class<Estados> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public EstadosImpl() {
		super(Estados.class);
	}
}
