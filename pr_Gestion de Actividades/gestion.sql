create database gestion;
use gestion;

create table personas(
id int not null auto_increment primary key,
nombre varchar(60) not null,
apellido varchar(60)not null,
dui varchar(10) not null unique,
telefono varchar(9),
correo varchar(120) not null unique
)engine InnoDB;

create table tipo_usuarios(
id int not null auto_increment primary key,
nombre varchar(15) not null	unique	
)engine InnoDB;

create table usuarios(
id int not null auto_increment primary key,
usuario varchar(25) not null unique,
pass blob not null,
persona int not null unique,
t_usuario int not null,
foreign key(persona) references personas(id),
foreign key(t_usuario) references tipo_usuarios(id)
)engine InnoDB;

create table estados(
id int not null auto_increment primary key,
nombre varchar(12) not null unique							/*en espera, en proceso, finalizado*/
)engine InnoDB;

create table actividades(
id int not null auto_increment primary key,
nombre varchar(100) not null,
descripcion varchar(600) not null,
fecha_fin_a date not null,
usuario int not null,
estado int not null,
foreign key(estado) references estados(id),
foreign key(usuario) references usuarios(id)
)engine InnoDB;

create table faces(
id int not null auto_increment primary key,
nombre varchar(12) not null	unique							/*inicio, proceso, finalizado*/
)engine InnoDB;

create table det_faces(
id int not null auto_increment primary key,
face int not null,
fecha_fin_f date,
actividad int not null,
foreign key(face) references faces(id),
foreign key(actividad) references actividades(id)
)engine InnoDB;

create table fecha_especial(
id int not null auto_increment primary key,
asueto date not null unique
)engine InnoDB;


